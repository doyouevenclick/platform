{% if grains['kernel'] == 'Linux' %}

# Use the BBR algorithm for TCP congestion control.
#  BBR is the only algorithm that avoids persistent packet queues (bufferbloat)
#  while maximising useable bandwidth.
tcp_bbr:
  kmod.present:
    - persist: yes

net.ipv4.tcp_congestion_control:
  sysctl.present:
    - value: bbr

# Use the CoDel queue management algorithm
# This is pretty efficient at limiting bufferbloat on busy links.
net.core.default_qdisc:
  sysctl.present:
    - value: fq_codel

{% endif %}
