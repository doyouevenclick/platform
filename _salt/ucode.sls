{% set ucode_pkg = "intel-microcode" %}

# Install Intel microcode updates
{{ ucode_pkg }}: pkg.installed

# Grub configuration
/etc/default/grub:
  file.line:
    - content: >-
        GRUB_CMDLINE_LINUX="spec_store_bypass_disable=auto security=apparmor"
    - match: >-
        ^#? *GRUB_CMDLINE_LINUX=
    - mode: replace
    - watch_in:
        - cmd: update-grub

update-grub:
  cmd.wait
