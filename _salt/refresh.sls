repo-update:
 schedule.present:
   - function: pkg.list_upgrades
   - job_kwargs:
       refresh: true
   - hours: 24
   - splay: 3600

highstate:
 schedule.present:
   - function: state.highstate
   - hours: 12
   - splay: 7200
