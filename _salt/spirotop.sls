prod:
  "G@os:Amazon":
     - match: compound
     - aws

  "G@os:Debian and G@cpuinfo:vendor_id:GenuineIntel":
    - ucode

  'master':
    - reactor.top
    - miniondns
    - salt.master
    - vpn.master

  "G@os:Debian":
    - debian.repos

  '*':
    - python
    - salt.minion
    - network
    - nfs
    - ssh
    - refresh
    - locale
    - tools
    - vpn

  'not brigid':
    - hostname
