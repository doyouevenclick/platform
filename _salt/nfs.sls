{% if grains['os_family'] == 'Debian' %}
nfs-kernel-server: pkg.purged
rpcbind: pkg.purged

{% elif grains['os_family'] == 'RedHat' %}
nfs-utils: pkg.purged
rpcbind: pkg.purged

{% endif %}
