update_dns:
  local.state.apply:
    - tgt: master
    - args:
      - mods: master.miniondns
      - saltenv: base
