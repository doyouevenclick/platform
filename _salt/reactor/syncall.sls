sync_all:
  local.saltutil.sync_all:
    - tgt: {{ data['id'] }}

mine_update:
  local.mine.update:
    - tgt: {{ data['id'] }}
