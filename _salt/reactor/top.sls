include:
- salt.master

/etc/salt/master.d/reactor.conf:
  file.serialize:
    - formatter: yaml
    - watch_in:
      - service: salt-master
    - dataset:
        reactor: []
          # - 'salt/minion/*/start':
          #   - salt://reactor/updatedns.sls?saltenv=platform/prod
          #   - salt://reactor/syncall.sls?saltenv=platform/prod

          # - 'salt/cloud/*/destroyed':
          #   - salt://reactor/updatedns.sls?saltenv=platform/prod
