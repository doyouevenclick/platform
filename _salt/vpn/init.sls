{% set netname = "salt" %}
{% set ifname  = "wg-" + netname %}
{% set key_file = "/etc/wireguard/" + ifname %}

{% if grains['os_family'] == 'Debian' %}
{% set wg_pkg = "wireguard" %}
{% set wg_svc = "wg-quick@" + ifname %}

{% if grains['osmajorrelease'] < 11 %}
kernel headers:
  pkg.installed:
    - name: linux-headers-{{ grains['osarch'] }}

wireguard-dkms:
  pkg.installed:
    - refresh: True
    - requires:
        - pkg: kernel headers
{% endif %}
{% endif %}

# Wireguard configuration

{{ wg_pkg }}:
  pkg.installed:
    - refresh: True

Generate keypair:
  cmd.run:
    - name: umask 277; wg genkey > {{ key_file }}
    - creates: {{ key_file }}

/etc/wireguard/{{ ifname }}.conf:
  file.managed:
    - source: salt://vpn/{{ 'master' if grains['id'] == 'master' else 'minion' }}.conf.j2
    - template: jinja
    - defaults:
        wg_port: 28469  # Wireguard port on the management server; uses a random port on minions
        key_file: {{ key_file }}
    - mode: 0400
#    - check_cmd: wg-quick strip
    - watch:
        - cmd: Generate keypair

{{ wg_svc }}:
  service.running:
    - enable: yes
    - watch:
        - file: /etc/wireguard/{{ ifname }}.conf
    - requires:
        - pkg: {{ wg_pkg }}

{% import_yaml 'vpn/ipam.yml' as addresses %}
/etc/salt/minion.d/vpn.conf:
  file.serialize:
    - formatter: yaml
    - watch_in:
      - service: salt-minion
    - dataset:
        master: {{ addresses['master'] }}
        mine_functions:
          salt_vpn.pubkey:
            - mine_function: cmd.run
            - 'sh -c "wg pubkey < {{ key_file }} || :"'


# Enable reverse-path filters, so we reject incoming packets whose purported source address
#  doesn't match the interface.  This is important to ensure the Wireguard IPs can be trusted
#  as machine identities: otherwise, a malicious machine on any local network (as opposed to
#  the Wireguard interfaces) can send packets with a spoofed source address.
{% for i in ['all', 'default'] %}
net.ipv4.conf.{{ i }}.rp_filter:
  sysctl.present:
    - value: 1
{% endfor %}
