{% import_yaml 'vpn/ipam.yml' as addresses %}
/etc/salt/master.d/vpn.conf:
  file.serialize:
    - formatter: yaml
    - watch_in:
      - service: salt-master
    - dataset:
        interface: {{ addresses['master'] }}
