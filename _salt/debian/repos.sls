{% if grains['id'] in ['master', 'statichost-01'] %}
{% set mirror = "https://debian.ffgraz.net" %}
{% else %}
{% set mirror = "https://deb.debian.org" %}
{% endif %}

{% set release = grains['oscodename'] %}

# Saltstack doesn't set osmajorrelease on testing/sid, so let's make it 666
{% set major_release = grains.get('osmajorrelease', 666) %}
{% if major_release > 10 %}
# The name of the suite for security updates changed in Bullseye (Debian 11)
# see: https://lists.debian.org/debian-devel-announce/2019/07/msg00004.html
{% set security_suite = release + "-security" %}
{% else %}
{% set security_suite = release + "/updates" %}
{% endif %}

/etc/apt/sources.list:
    file.absent

/etc/apt/sources.list.d/{{release}}.list:
  file.managed:
    - contents: |
        deb     {{ mirror }}/debian {{ release }} main contrib non-free
        deb-src {{ mirror }}/debian {{ release }} main contrib non-free

        deb     {{ mirror }}/debian-security {{ security_suite }} main contrib non-free
        deb-src {{ mirror }}/debian-security {{ security_suite }} main contrib non-free

{% if grains['osmajorrelease'] < 11 %}
# Wireguard is not available on buster, only buster-backports
/etc/apt/sources.list.d/{{release}}-backports.list:
  file.managed:
    - contents: |
        deb     {{ mirror }}/debian {{ release }}-backports main contrib non-free
        deb-src {{ mirror }}/debian {{ release }}-backports main contrib non-free
{% endif %}

/etc/apt/apt.conf.d/70release:
  file.managed:
    - contents: |
        APT::Default-Release "{{ release }}";
