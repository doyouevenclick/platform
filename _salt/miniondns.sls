#!pyobjects

if grains('lsb_distrib_release') == 'Debian':
    if grains('lsb_distrib_release') <= 10:
        Pkg.installed('dnsutils')
    else:
        Pkg.installed('bind9-dnsutils')

else:
    # This really should emit a warning
    pass

def record(host, type, value):
    if value:
        BotoRoute53.present(
            "%s_%s" % (host, type),
            name=host,
            value=value,
            zone="doyoueven.click",
            record_type=type,
            ttl=(60),
        )
    else:
        BotoRoute53.absent(
                "%s_%s" % (host, type),
                name=host,
                zone="doyoueven.click",
                record_type=type,
            )


def alias(target, source, nameserver=None):
    ip4 = salt.dnsutil.A(source, nameserver=nameserver)
    if ip4:
        record(target, "A", ip4)

    ip6 = salt.dnsutil.AAAA(source, nameserver=nameserver)
    if ip6:
        record(target, "AAAA", ip6)

with BotoRoute53.hosted_zone_present(
    "doyoueven.click.",
    domain_name="doyoueven.click.",
    comment="",
):
    for mid, ips in mine('*', 'ipaddr.four').items():
        record(mid+'.doyoueven.click.', 'A', ips)

    for mid, ips in mine('*', 'ipaddr.six').items():
        record(mid+'.doyoueven.click.', 'AAAA', ips)
    
    if hasattr(salt.ipaddr, 'four'):
        record('salt.doyoueven.click', 'A', salt.ipaddr.four())
    #if hasattr(salt.ipaddr, 'six'):
    #    record('salt.doyoueven.click', 'AAAA', salt.ipaddr.six())
    record('salt.doyoueven.click', 'AAAA', None)

    Schedule.present('dns-refresh',
        function='state.apply',
        job_kwargs={
            'mods': 'minionds',
            'saltenv': 'platform/prod',
        },
        minutes=5,
        splay=60,

    )

    record("doyoueven.click", "MX", [
        "10 in1-smtp.messagingengine.com.",
        "20 in2-smtp.messagingengine.com.",
    ])

    record("fm1._domainkey.doyoueven.click", "CNAME", 'fm1.doyoueven.click.dkim.fmhosted.com')
    record("fm2._domainkey.doyoueven.click", "CNAME", 'fm2.doyoueven.click.dkim.fmhosted.com')
    record("fm3._domainkey.doyoueven.click", "CNAME", 'fm3.doyoueven.click.dkim.fmhosted.com')
