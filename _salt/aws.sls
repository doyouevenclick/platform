include:
- salt.minion

{# This is installed with spiro-network, leave it off for now.
requests:
  pip.installed:
    - bin_env: {{pipbin}}
    - watch_in:
      - service: salt-minion
#}

/etc/salt/minion.d/metadata.conf:
  file.serialize:
    - formatter: yaml
    - watch_in:
      - service: salt-minion
    - dataset:
        metadata_server_grains: True
