#!pyobjects
# This assumes fqdn = hostname.networkname
# This is also for Linux, which gets the fqdn by resolve-unresolve (hostname -> ip -> fqdn)
# (When reading this, keep in mind state compilation vs evaluation)

current_hostname = salt.network.get_hostname()
current_fqdn = salt.network.get_fqdn()
target_hostname = grains('id')
target_domain = 'doyoueven.click'
target_fqdn = target_hostname + '.' + target_domain

self_ip = salt.hosts.get_ip(current_hostname)
if not self_ip:
    try:
        ips = salt.network.ip_addrs(type='public')[0]
    except IndexError:
        pass

if not self_ip:
    self_ip = '127.0.1.1'  # This seems to be the default for Debian and Arch

try:
    getattr(Hostname, 'is')(target_hostname)
except NameError:
    # Module not loaded yet
    pass

Host.only(self_ip, hostnames=[target_fqdn, target_hostname])
Sysctl.present('kernel.domainname', value=target_domain)
