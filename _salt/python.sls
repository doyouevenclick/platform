{% if grains['os_family'] == 'Debian' %}
python: pkg.installed
python-pip: pkg.installed

python3: pkg.installed
python3-pip: pkg.installed
python3-venv: pkg.installed
{% endif %}
