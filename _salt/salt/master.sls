salt-master:
  service.running:
    - enable: yes

/etc/salt/master:
  file.absent

/etc/salt/master.d/base.conf:
  file.serialize:
    - formatter: yaml
    - watch_in:
      - service: salt-master
    - dataset:
        cachedir: /srv/salt/cache/master

        # Saltstack 2018.3 introduced a master autodiscovery feature,
        #  later enabled by default, that exposes a new port (4520)
        #  to all interfaces, regardless of the `interface` setting.
        discovery: no

/etc/logrotate.d/salt-common:
  file.managed:
    # Taken from Salt's repo, pkg/salt-common.logrotate
    - source: salt://salt/salt-common.logrotate
    - mode: 0644
    - watch_in:
        - service: logrotate

logrotate-pkg:
  pkg.installed:
    - name: logrotate

logrotate:
  service.running
