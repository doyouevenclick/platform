include:
- python

{% if grains['os'] in ('Debian', 'Raspbian') %}
{% set pipbin = "/usr/bin/pip3" %}

# Saltstack doesn't set osmajorrelease on testing/sid, so let's make it 666
{% set release = grains['osmajorrelease'] %}
{% if release < 10 %}
# apt supports HTTPS by default, starting in Buster
apt-transport-https: pkg.installed
{% endif %}

{% set saltversion = "3002" %}

salt-repo:
  pkgrepo.managed:
    - name: 'deb https://repo.saltstack.com/py3/debian/{{release}}/{{grains['osarch']}}/{{saltversion}} {{grains['oscodename']}} main'
    - key_url: https://repo.saltstack.com/py3/debian/{{release}}/{{grains['osarch']}}/{{saltversion}}/SALTSTACK-GPG-KEY.pub
    - file: /etc/apt/sources.list.d/saltstack.list

salt-minion:
  pkg.installed: []
  service.running:
    - enable: yes


{% else %}
{% set pipbin = "/usr/bin/pip3" %}

salt-minion:
  service.running:
    - enable: yes

{% endif %}

spiro-ip:
  pip.removed:
    - bin_env: {{pipbin}}

spiro-network:
  pip.installed:
    - bin_env: {{pipbin}}
    - watch_in:
      - service: salt-minion

/etc/salt/minion:
  file.absent

/etc/salt/minion.d/base.conf:
  file.serialize:
    - formatter: yaml
    - watch_in:
      - service: salt-minion
    - dataset:
        top_file_merging_strategy: same  # spiro does this anyway, but shuts up a warning

        master_tries: -1  # keep retrying the master indefinately
        random_startup_delay: 10  # seconds
        ping_interval: 1  # minutes
        tcp_keepalive: True
        tcp_keepalive_cnt: 6
        tcp_keepalive_idle: 10  # seconds
        tcp_keepalive_intvl: 10  # seconds

{% if grains['id'] == 'master' %}
        cachedir: /srv/salt/cache/minion
{% endif %}

        # Saltstack 2018.3 introduced a master autodiscovery feature,
        #  later enabled by default, that causes the minion to broadcast
        #  for a master; we never want this in our setup.
        discovery: no

        mine_functions:
          ipaddr.four: []
          ipaddr.six: []
          ssh.host_keys:
            private: false
            certs: false

/etc/systemd/system/salt-minion.service.d/kill.conf:
  file.managed:
    - mode: 0444
    - makedirs: yes
    - dir_mode: 0755
    - watch_in:
        - service: salt-minion

    - contents: |
        [Service]
        # Ensure that no process outlives the minion's main process
        # Leftover processes can kill the new minion and cause a kill-loop
        KillMode=mixed

        # Always restart salt-minion when it exits:
        #  we never want an unmanageable system.
        Restart=always
