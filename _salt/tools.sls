{% if grains['os'] in ('Debian', 'Raspbian') %}
{% set packages = ['ldnsutils', 'htop', 'mosh', 'mtr-tiny', 'tcpdump'] %}
{% endif %}

{% for pkg in packages %}
{{ pkg }}:
  pkg.installed
{% endfor %}
