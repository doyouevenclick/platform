{% if grains['os_family'] == 'Debian' %}
{% set openssh_pkg = "openssh-server" %}
{% endif %}

# SSH client configuration

/etc/ssh/ssh_config:
  file.managed:
    - source: salt://ssh/ssh_config.j2
    - template: jinja
    - mode: 0444

/etc/ssh/known_hosts.d/doyoueven.click:
  file.managed:
    - source: salt://ssh/known_hosts.j2
    - template: jinja
    - mode: 0444
    - makedirs: yes
    - dir_mode: 0755


# SSH server configuration

{{ openssh_pkg }}: pkg.installed

{% for algo in ['dsa', 'ecdsa'] %}
/etc/ssh/ssh_host_{{ algo }}_key:     file.absent
/etc/ssh/ssh_host_{{ algo }}_key.pub: file.absent
{% endfor %}

/etc/ssh/sshd_config:
  file.managed:
    - source: salt://ssh/sshd_config.j2
    - template: jinja
    - mode: 0444
    - check_cmd: /usr/sbin/sshd -t -f

sshd:
  service.running:
    - enable: yes
    - watch:
        - file: /etc/ssh/sshd_config
        - pkg: {{ openssh_pkg }}
